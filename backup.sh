#!/bin/bash
source ../Secrets/cisco.creds
echo "***Backing up Cisco switches***"
ansible-playbook cisco.backup.yml
